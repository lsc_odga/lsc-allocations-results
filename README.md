# Basic Field Allocations Results page on lsc.gov

## About
There are a few pages on lsc.gov related to Basic Field Grant allocations (process, results, data files). This repository is for the page about the RESULTS. The page uses DataTables library.

The page has been overhauled in style and in the design of the allocation bar charts to better fit in with the new lsc.gov website.

## Developing
Run `npm run start` for a local development version. (You may need to first run `nvm use 14` or some other version if the latest node version gives issues.)

The file lsc_gov_base.scss is intended to be built up and re-used across projects to replicate styles on lsc.gov and reduce styling and layout surprises when uploading the code onto lsc.gov.

JSON files are from the web-data-public folder in AWS. These require manual update each year.

## Publishing
Run `npm run build` to process, bundle, auto-prefix (etc.) the code for use in production. Assets, JS, and CSS files in production are pulled from our CDN on AWS. Use https://d29f85wyq8xezh.cloudfront.net/lsc-website/assets for general lsc.gov icons (etc.). Take all files and the assets directory compiled to the dist folder and upload them to the CDN folder for this project at https://d29f85wyq8xezh.cloudfront.net/lsc-website/lsc-allocations-results/. Except for the index.html file. (Note that the "public path" option in the webpack configuration handles assigning this path to all assets, JS, and CSS files referenced in the compiled files.)

With the index.html, you can upload it to a test static site in S3 to ensure that the resources are being pulled correctly from the CDN. Ultimately, copy the index.html contents into a raw HTML block on lsc.gov and test there. (Ensure that there are no surprises in styling and layout due to being located on lsc.gov; edit and recompile as needed).

## Updating
The only file that needs updating is in the AWS S3 bucket web-data-public at poverty-and-allocations/poverty_allocations_and_changes_by_year.json. There is an R script to take the final CSV that results from each year's allocation process and output the JSON with % change, reason for change, etc. See the [Box folder](https://lsc-live.app.box.com/folder/124049703267) at ODGA/Communications/Website Materials/Publicly Shared Files/Grant Award Calculations/_preparation.