import "core-js/stable";
import "regenerator-runtime/runtime.js";
import "../css/main.scss";
import "../css/dataTablesCustom.scss";
var $ = require('jquery');
import { mouseAndTabListener } from '../js/utils/accessibility.js';

// if (!window.fetch) {
//   fetch.push(import(/* webpackChunkName: "polyfill-fetch" */ 'whatwg-fetch'))
// }

mouseAndTabListener();

$(document).ready(function() {
	fetch("https://lt2v6wga3l.execute-api.us-east-1.amazonaws.com/default/lsc-data-api?key=poverty-and-allocations/poverty_allocations_and_changes_by_year.json")
	.then(function(response) { return response.json(); })
	.then(function(response) { 
		let r = response;
		import('./drawDataTable.js')
		.then(function(module) {
			module.drawDataTable(r);
			$('.loader-container').css('display', 'none');
		}).catch(function(error) { 
			console.log(error); 
		});
	});
});




