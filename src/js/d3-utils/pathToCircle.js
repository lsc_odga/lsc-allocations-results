const d3_polygon = require('d3-polygon');

//PATH TO CIRCLE. Based on:
//https://bl.ocks.org/mbostock/3081153
//https://bl.ocks.org/HarryStevens/fdf207fcb3f318288c0dbef3ec7d3ee8

export let zoomStateRadius;
export let zoomStateCentroid;
export function pathToCircle(coordinates) {

  let circle = [],
      n = coordinates.length,
      lengths = [0],
      p0 = coordinates[0],
      p1,
      x,
      y,
      i = 0;

  // Compute the distances of each coordinate.
  while (++i < n) {
    p1 = coordinates[i];
    x = p1[0] - p0[0];
    y = p1[1] - p0[1];
    lengths.push(length += Math.sqrt(x * x + y * y));
    p0 = p1;
  }

  let area = d3_polygon.polygonArea(coordinates);
  area < 0 ? area = area * -1 : area = area;
  zoomStateRadius = Math.sqrt(area) / 2.5;
  zoomStateCentroid = d3_polygon.polygonCentroid(coordinates);
  let angleOffset = Math.PI,
      angle,
      j = 0,
      k = 2 * Math.PI / lengths[lengths.length - 1];

  // Compute points along the circle’s circumference at equivalent distances.
  while (++j < n) {
    angle = angleOffset + lengths[j] * k;
    circle.push([
      zoomStateCentroid[0] + zoomStateRadius * Math.cos(angle),
      zoomStateCentroid[1] + zoomStateRadius * Math.sin(angle)
    ]);
  }

  return circle;
}
