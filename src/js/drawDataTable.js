var $ = require('jquery');
//note, this slightly differs from the instructions: see https://datatables.net/forums/discussion/53880/cant-get-this-to-work-with-npm-and-webpack
import jsZip from 'jszip';
require( 'datatables.net-dt' );
require( 'datatables.net-buttons-dt' );
require( 'datatables.net-buttons/js/buttons.flash.js' );
require( 'datatables.net-buttons/js/buttons.html5.js' );
require( 'datatables.net-fixedcolumns-dt' );
require( 'datatables.net-fixedheader-dt' );
require( 'datatables.net-responsive-dt' );
require( 'datatables.net-rowgroup-dt' );
import { fundingChangeExplanations } from './fundingChangeExplanations.js';
import { spaceEnterOrClick } from '../js/utils/accessibility.js';

window.JSZip = jsZip; //Finally found the problem with the missing Excel button: https://stackoverflow.com/questions/37966718/datatables-export-to-excel-button-is-not-showing
const d3_format = require('d3-format');

export function drawDataTable(dataFile) {
	let data = dataFile.data;

	//Round some of the data. Build some lists for dropdowns.
	let grantees = [];
	let years = [];
	let saTypes = ['Agricultural Worker', 'Basic Field General', 'Native American'];
	let fundingChangeCodes = [];

	data.forEach(function (d) {
    if (d.poverty) { d.poverty = Math.round(+d.poverty) };
		if (d.pov_chg_from_prior_yr) { d.pov_chg_from_prior_yr = Math.round(+d.pov_chg_from_prior_yr) };

		if (d.service_area_type == 'Basic Field') { d.service_area_type = 'Basic Field General'}

		if (grantees.indexOf(d.current_org) < 0) {
			grantees.push(d.current_org)
		}
		if (years.indexOf(d.fy) < 0) {
			years.push(d.fy)
		}
		if (d.funding_change_code && fundingChangeCodes.indexOf(d.funding_change_code) < 0) {
			fundingChangeCodes.push(d.funding_change_code)
		}
	});
	
	//POPULATE SOME SELECT MENUS
	grantees.sort();
	years.sort(function(a, b){return b-a});
	fundingChangeCodes.sort();

	for (var i = 0; i < grantees.length; i++) {
		$('<option/>', {
					value: grantees[i],
					html: grantees[i]
					}).appendTo('#grantee-select-menu select');
	}

	for (var i = 0; i < years.length; i++) {
		$('<option/>', {
					value: years[i],
					html: years[i]
					}).appendTo('#year-select-menu select'); 
	}

	for (var i = 0; i < saTypes.length; i++) {
		$('<option/>', {
					value: saTypes[i],
					html: saTypes[i]
					}).appendTo('#sa-type-select-menu select'); 
	}


	//Function to render formatted versions of the values while retaining the number value beneath for downloads
	$.fn.dataTable.render.formatDisplayText = function (format) {
		return function ( data, type, row ) {
        if ( type === 'display' ) {
					var num = data;
					if (num !== undefined && format !== undefined) { 
						num = d3_format.format(format)(data); 
					} else if (num == undefined) {
						num = '--'
					}
					return num;
				}
        return data; //leave the non-display data the same
			};
	};


	let maxTableHeight = 400;
	let defaultColWidth = "6rem",
			maxColWidth = "14rem";

	let dataTable = $('#dataTableTable').DataTable( {
		// paging: false,
		scrollX: true,
		sDom: "Bfrtip",
		pageLength: 10,
		// scrollY: maxTableHeight,
		scrollCollapse: true,
		deferRender: true,
		// fixedHeader: true,
		data : data,
		columns : [
			{ data : "service_area", title: "Service Area", defaultContent: "", width: "5rem", className: "col-text-center" },
			{ data : "service_area_type", title: "Grant Component", width: "8rem", defaultContent: ""},
			{ data : "current_org", title: "Grantee", width: maxColWidth, defaultContent: "" },
			{ data : "fy", title: "Fiscal Year", defaultContent: "", width: "3rem", className: "col-text-center"},
			{ data : "allocation", title: "Allocation", defaultContent: "", width: defaultColWidth, className: "col-text-right col-allocation-highlight", render: $.fn.dataTable.render.formatDisplayText(',')},
			{ data : "poverty", title: "Poverty Pop.<sup>1</sup>", defaultContent: "", width: defaultColWidth, className: "col-text-right", render: $.fn.dataTable.render.formatDisplayText(',') },
			{ data : "pov_share", title: "Share of U.S. Poverty Pop.", defaultContent: "", width: defaultColWidth, className: "col-text-right", render: $.fn.dataTable.render.formatDisplayText('.3%')},
			{ data : "allocation_chg_from_prior_yr", title: "Allocation Change from Prior Yr", defaultContent: "", width: defaultColWidth, className: "col-text-right", render: $.fn.dataTable.render.formatDisplayText(',')},
			{ data : "pov_chg_from_prior_yr", title: "Poverty Pop Change from Prior Yr", defaultContent: "", width: defaultColWidth, className: "col-text-right", render: $.fn.dataTable.render.formatDisplayText(',')},
			{ data : "pov_share_chg_from_prior_yr", title: "Poverty Share Change from Prior Yr", defaultContent: "", width: "7rem", className: "col-text-right", render: $.fn.dataTable.render.formatDisplayText()},
			{ data : "funding_change_code", title: "Allocation Change Explanation<sup>2</sup>", defaultContent: "", width: defaultColWidth, className: "col-text-center", render: $.fn.dataTable.render.formatDisplayText()}
		],
		buttons: {
      buttons: [
        { extend: 'csv', className: 'download-button', exportOptions: {columns: ':visible'}, title: 'Basic Field Allocations' },
				{ extend: 'excel', className: 'download-button', exportOptions: {columns: ':visible'}, title: 'Basic Field Allocations' }      
			]
		}
		// "initComplete": function(settings, json) {
		// 	$('.dataTables_scrollBody thead tr').css({visibility:'collapse'});
		// },
		// "drawCallback": function () { $('.dataTables_scrollBody thead tr').css({ visibility: 'collapse' }); }
	});


	function filterTheTable(val, col) {
		if (val !== 'ALL') {
			dataTable
				.column(col).search( val ).draw();
		} else {
			dataTable.column(col).search('').draw();
		}
	}

	$('#grantee-select-menu select').on('change',function(){
		let val = $(this).val();
		filterTheTable(val, 2);
	});

	$('#year-select-menu select').on('change',function(){
		let val = $(this).val();
		filterTheTable(val, 3);
	});

	$('#sa-type-select-menu select').on('change',function(){
		let val = $(this).val();
		filterTheTable(val, 1);
	});


	//ADD funding change codes section
	fundingChangeCodes.forEach(function(d) {
		$('#funding-change-buttons').append(`<div class="funding-change-button" role="button"  tabindex="0" aria-expanded="false" aria-controls="funding-change-explanation"><span class="funding-change-letter">${d}</span></div>`);
	});

	$('.funding-change-button').on('click keypress', function(event) {
		if (spaceEnterOrClick(event) === true) {
			$('.funding-change-button').removeClass('funding-change-button-selected');
			$(this).addClass('funding-change-button-selected');
			
			$('.funding-change-button').attr('aria-expanded', "false");
			$(this).attr('aria-expanded', "true");
	
			let letter = $(this).find('.funding-change-letter').html();
	
			$('#funding-change-explanation').css('visibility', 'visible');
			$('#funding-change-explanation').html(fundingChangeExplanations[letter]);
		}
	});

	return;
}