const path = require("path");
const common = require("./webpack.common");
const { merge } = require("webpack-merge");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
var HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = merge(common, {
  mode: "production",
  output: {
    // filename: "[name].[contentHash].bundle.js",
    filename: "[name].bundle.js",
		path: path.resolve(__dirname, "dist/"),
		publicPath: "https://d29f85wyq8xezh.cloudfront.net/lsc-website/lsc-allocations-results/"
  },
  optimization: {
    splitChunks: { chunks: "all" },
    minimizer: [
      new OptimizeCssAssetsPlugin(),
      new TerserPlugin(),
      new HtmlWebpackPlugin({
				template: "./src/index.html",
        minify: {
          removeAttributeQuotes: true,
          collapseWhitespace: true,
          removeComments: true
        }
			})
    ]
  },
  plugins: [
    // new MiniCssExtractPlugin({ filename: "[name].[contentHash].css" }),
    new MiniCssExtractPlugin({ filename: "[name].css" }),
    new CleanWebpackPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          "sass-loader"
        ]
      },
      {
        test: /\.js$/,
        include: [path.resolve(__dirname, "src/"), path.resolve(__dirname, 'node_modules/d3-format')], //d3-format's arrow functions argh!
        use: {
					loader: "babel-loader"
				}
      }
    ]
  }
});
